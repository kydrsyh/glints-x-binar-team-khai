const index = require('./index.js');

function isEmptyOrSpaces(str) {
    return str === null || str.match(/^ *$/) !== null;
}

function calculateCuboid(length, width, height) {
    return length * width * height
}

function inputLength() {
    index.rl.question(`Length: `, length => {
        if (!isNaN(length) && !isEmptyOrSpaces(length)) {
            inputWidth(length);
        } else {
            console.log("Length must be a number !\n");
            inputLength();
        }
    })
}

function inputWidth(length) {
    index.rl.question(`Width: `, width => {
        if (!isNaN(width) && !isEmptyOrSpaces(width)) {
            inputHeight(length, width);
        } else {
            console.log("Width must be a number !\n");
            inputWidth(length);
        }
    })
}

function inputHeight(length, width) {
    index.rl.question(`Height: `, height => {
        if (!isNaN(height) && !isEmptyOrSpaces(height)) {
            console.log(`The Cuboid's volume is ${calculateCuboid(length, width, height)} m3\n`);
            index.rl.close()
        } else {
            console.log("Height must be a number !\n");
            inputHeight(length, width);
        }
    })
}

module.exports.input = inputLength