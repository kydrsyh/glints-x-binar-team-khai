const express = require('express')
const router = express.Router()

//Assignment Group
const pemasokController = require('../controllers/pemasokController.js')

router.get('/', pemasokController.getAll)
router.get('/:id', pemasokController.getOne)
router.post('/create', pemasokController.create)
router.put('/update/:id', pemasokController.update)
router.delete('/delete/:id', pemasokController.delete)
module.exports = router