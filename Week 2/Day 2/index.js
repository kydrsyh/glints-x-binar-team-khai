const readline = require('readline')
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
})

const tube = require('./sahlan.js')
const cuboid = require('./key.js')
const cube = require('./sherary.js')
const prism = require('./eka.js')

function menu() {
    console.log("Menu");
    console.log("====");
    console.log("1. Tube"); // Calculate Tube (Sahlan)
    console.log("2. Cube"); // Calculate Cube (Sherary)
    console.log("3. Cuboid"); // Calculate Cuboid (Key)
    console.log("4. Prism"); // Calculate Prism (Eka)
    console.log("5. Exit");
    rl.question(`Choose option: `, option => {
        if (!isNaN(option)) {
            if (option == 1) {
                tube.input()
            } else if (option == 2) {
                cube.input()
            } else if (option == 3) {
                cuboid.input()
            } else if (option == 4) {
                prism.input()
            } else if (option == 5) {
                console.log("See ya!");
                rl.close()
            } else {
                console.log("Option must be 1 to 5");
                menu()
            }
        } else {
            console.log("Option must be a number !");
            menu()
        }
    })
}

menu()


module.exports.rl = rl