const index = require('./index.js');

function isEmptyOrSpaces(str) {
    return str === null || str.match(/^ *$/) !== null;
}

function cubeVolume(length) {
    return length ** 3;
}

function inputLength() {
    index.rl.question(`Length: `, length => {
        if (!isNaN(length) && !isEmptyOrSpaces(length)) {
            console.log(`The volume of the cube is ${cubeVolume(length)} cm^3\n`)
            index.rl.close();
        } else {
            console.log("Length must be a number!\n")
            inputLength();
        }
    })
}

module.exports.input = inputLength