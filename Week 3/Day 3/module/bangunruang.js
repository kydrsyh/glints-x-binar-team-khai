// Import Bangun  class
const bangun = require('./bangun.js');

// This is class BangunDatar
class BangunRuang extends bangun {
    constructor(name) {
      super(name)
  
      // It is abstract class
      if (this.constructor === BangunRuang) {
        throw new Error('This is abstract')
      }
    }
  
    // This is private method, so you can't access on another js
    #hello() {
      console.log('Hello Bangun Ruang!');
    }
  
    // instance method
    menghitungLuas() {
      console.log(`Menghitung Luas`);
    }
  
    // instance method
    menghitungVolume() {
      console.log(`Menghitung Volume`);
    }
  }
  
  module.exports = BangunRuang;
  