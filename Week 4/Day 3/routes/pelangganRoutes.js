const express = require('express')
const router = express.Router()

//Assignment Group
const pelangganController = require('../controllers/pelangganController.js')

router.get('/', pelangganController.getAll)
router.get('/:id', pelangganController.getOne)
router.post('/create', pelangganController.create)
router.put('/update/:id', pelangganController.update)
router.delete('/delete/:id', pelangganController.delete)

module.exports = router