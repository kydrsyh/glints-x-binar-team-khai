const express = require('express')
const router = express.Router()

//Assignment Group
const BarangController = require('../controllers/barangController.js')

router.get('/', BarangController.getAll)
router.get('/:id', BarangController.getOne)
router.post('/create', BarangController.create)
router.put('/update/:id', BarangController.update)
router.delete('/delete/:id', BarangController.delete)

module.exports = router