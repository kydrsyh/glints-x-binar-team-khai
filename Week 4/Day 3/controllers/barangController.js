const connection = require('../models/connection.js')

class BarangController {
    async getAll(req, res) {
        try {
            var sql = "SELECT b.id AS id_barang, b.nama AS item, b.harga, p.nama AS pemasok FROM barang b JOIN pemasok p ON b.id_pemasok = p.id ORDER BY b.id"
            connection.query(sql, function (err, result) {
                if (err) {
                    res.json({
                        status: "Error",
                        error: err
                    });
                }
                res.json({
                    status: "success",
                    data: result
                })
            });
        } catch (err) {
            res.json({
                status: "Error",
                error: err
            })
        }
    }
    async getOne(req, res) {
        try {
            var sql = "SELECT b.id AS id_barang, b.nama AS item, b.harga, p.nama AS pemasok FROM barang b JOIN pemasok p ON b.id_pemasok = p.id WHERE b.id = ?"
            connection.query(sql, [req.params.id], function (err, result) {
                if (err) {
                    res.json({
                        status: "Error",
                        error: err
                    });
                }
                res.json({
                    status: "success",
                    data: result[0]
                })
            });
        } catch (err) {
            res.json({
                status: "Error",
                error: err
            })
        }
    }
    async create(req, res) {
        try {
            var sql = 'INSERT INTO barang(nama, harga, id_pemasok) VALUES (?, ?, ?)'
            connection.query(
                sql,
                [req.body.nama, req.body.harga, req.body.id_pemasok],
                (err, result) => {
                    if (err) {
                        res.json({
                            status: "Error",
                            error: err
                        });
                    }
                    var sqlInsert = "SELECT b.id AS id_barang, b.nama AS item, b.harga, p.nama AS pemasok FROM barang b JOIN pemasok p ON b.id_pemasok = p.id WHERE b.id = ?"
                    connection.query(sqlInsert, [result.insertId],(err, result) => {
                        if(err) {
                            res.json({
                                status: "Error",
                                error: err
                            })
                        }

                        res.json({
                            status: 'Success',
                            data: result
                        })
                    })
                  
                }
            )
        } catch (err) {
            res.json({
                status: "Error",
                error: err
            })
        }
    }
    async update(req, res) {
        try {
            var sql = 'UPDATE barang b SET nama = ?, harga = ?, id_pemasok = ? WHERE id = ?'

            connection.query(
                sql,
                [req.body.nama, req.body.harga, req.body.id_pemasok, req.params.id],
                (err, result) => {
                    console.log(req.params.id);
                    if (err) {
                        res.json({
                            status: "Error",
                            error: err
                        });
                    }
                    var sqlSelect = "SELECT b.id AS id_barang, b.nama AS item, b.harga, p.nama AS pemasok FROM barang b JOIN pemasok p ON b.id_pemasok = p.id WHERE b.id = ?"
                    connection.query(sqlSelect, [req.params.id], (err,result) => {
                        if(err) {
                            res.json({
                                status: "Error",
                                data: err
                            })
                        } res.json({
                            status: 'Success',
                            data: result
                        })
                    })
                    
                }
            )
        } catch (err) {
            res.json({
                status: "Error",
                error: err
            })
        }
    }
    async delete(req, res) {
        try {
            var sql = 'DELETE FROM barang b WHERE id = ?'
            connection.query(
                sql,
                [req.params.id],
                (err, result) => {
                    if (err) {
                        res.json({
                            status: "Error",
                            error: err
                        });
                    }
                    res.json({
                        status: 'Success',
                        data: result
                    })
                }
            )
        } catch (err) {
            res.json({
                status: "Error",
                error: err
            })
        }
    }
}

module.exports = new BarangController