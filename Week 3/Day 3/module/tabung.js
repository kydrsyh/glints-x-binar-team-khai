// import bangun ruang
const BangunRuang = require('./bangunruang.js')


// inherit the formula
class Tabung extends BangunRuang {
    constructor(tinggi, radius) {
        super('Tabung') 
        this.tinggi = tinggi
        this.radius = radius
    }

    // override menghitungLuas dari BangunRuang 
    menghitungLuas() {
        return Math.floor(2 * Math.PI * Math.pow(this.radius, 2) * this.tinggi * 100 )  / 100
    }

    // override volume
    menghitungVolume() {
        return Math.floor(Math.PI * Math.pow(this.radius, 2) * this.tinggi * 100 ) / 100
    }
}

module.exports = Tabung;