// Import bangun datar all class
const persegi = require('./persegi.js');
const persegipanjang = require('./persegipanjang.js');
const lingkaran = require('./lingkaran.js')
const kubus = require('./kubus.js')
const balok = require('./balok.js')
const tabung = require('./tabung.js')
const kerucut = require('./kerucut.js')

/* Start make module */
class Module {
  constructor() {
    this.name = 'Ini modul'
  }

  menghitungLuasPersegi(sisi) {
    let persegiHitung = new persegi(sisi) // make object of persegi
    return persegiHitung.menghitungLuas() // calculate area of persegi
  }

  menghitungKelilingPersegi(sisi) {
    let persegiHitung = new persegi(sisi) // make object of persegi
    return persegiHitung.menghitungKeliling() // calculate perimeter of persegi
  }

  menghitungLuasPersegiPanjang(panjang, lebar) {
    let persegiPanjangHitung = new persegipanjang(panjang, lebar) // make object of persegi panjang
    return persegiPanjangHitung.menghitungLuas() // calculate area of persegi panjang
  }

  menghitungKelilingPersegiPanjang(panjang, lebar) {
    let persegiPanjangHitung = new persegipanjang(panjang, lebar) // make object of persegi panjang
    return persegiPanjangHitung.menghitungKeliling() // calculate perimeter of persegi panjang
  }

  menghitungLuasLingkaran(radius) {
    let lingkaranHitung = new lingkaran(radius) // make object of lingkaran
    return lingkaranHitung.menghitungLuas() // calculate area of lingkaran
  }

  menghitungKelilingLingkaran(radius) {
    let lingkaranHitung = new lingkaran(radius) // make object of lingkaran
    return lingkaranHitung.menghitungKeliling()
  }

  // this is star a group assignment //
  menghitungLuasKubus(sisi) {
    let kubusHitung = new kubus(sisi)
    return kubusHitung.menghitungLuas()
  }
  menghitungVolumeKubus(sisi) {
    let kubusHitung = new kubus(sisi)
    return kubusHitung.menghitungVolume()
  }
  menghitungLuasBalok(length, width, height) {
    let balokHitung = new balok(length, width, height)
    return balokHitung.menghitungLuas()
  }
  menghitungVolumeBalok(length, width, height) {
    let balokHitung = new balok(length, width, height)
    return balokHitung.menghitungVolume()
  }

  menghitungLuasKerucut(radius, sisi, tinggi) {
    let kerucutHitung = new kerucut(radius, sisi, tinggi)
    return kerucutHitung.menghitungLuas()
  }
  menghitungVolumeKerucut(radius, sisi, tinggi) {
    let kerucutHitung = new kerucut(radius, sisi, tinggi)
    return kerucutHitung.menghitungVolume()
  }

  menghitungLuasTabung(tinggi, radius) {
    let tabungHitung = new tabung(tinggi, radius)
    return tabungHitung.menghitungLuas()
  }
  menghitungVolumeTabung(tinggi, radius) {
    let tabungHitung = new tabung(tinggi, radius)
    return tabungHitung.menghitungVolume()
  }



  

}
/* end make module */

module.exports = Module;
