// import modul
const modul = require('./module/module.js');

// instansiasi modul
const hitung = new modul()

/* Start group assignment below this line */
/* Khaidarsyah */
console.log("Assignment Class");
console.log("----------------------");
console.log("Khaidarsyah : Balok");
console.log("Luas   :", hitung.menghitungLuasBalok(10, 20, 30), "m2");
console.log("Volume :", hitung.menghitungVolumeBalok(10, 20, 30), "m3");
console.log("");

/* Sahlan */
console.log("Sahlan : Kubus");
console.log("----------------------");
console.log("Luas   :", hitung.menghitungLuasKubus(10), "m2");
console.log("Volume :",hitung.menghitungVolumeKubus(10), "m3");
console.log("");

/* Sherary */
console.log("Sherary : Tabung");
console.log("----------------------");
console.log("Luas   : ", hitung.menghitungLuasTabung(10, 14), "m2");
console.log("Volume : ", hitung.menghitungVolumeTabung(10, 14), "m3");
console.log("");

/* Eka */
console.log("Eka : Kerucut");
console.log("----------------------");
console.log("Luas   : ", hitung.menghitungLuasKerucut(7, 10, 8), "m2");
console.log("Volume : ", hitung.menghitungVolumeKerucut(7, 10, 8), "m3");
console.log("");