// Import bangun ruang
const BangunRuang = require('./bangunruang.js');
const bangunruang = require('./bangunruang.js');

// Make Kubus class inherit from BangunRuang
class Kubus extends BangunRuang {
  constructor(sisi) {
    super('Kubus')

    this.sisi = sisi
  }

  // override menghitungLuas BangunRuang class
  menghitungLuas() {
    // super.menghitungLuas()
    return Math.pow(this.sisi, 2) * 6
  }

  // override menghitungVolume BangunRuang class
  menghitungVolume() {
    return Math.pow(this.sisi, 3)
  }
}

module.exports = Kubus;
