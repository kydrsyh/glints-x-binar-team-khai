const data = require('./arrayFactory.js');
const test = require('./test.js');

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter(i => typeof i === 'number');
}  

// Should return array
function sortAscending(data) {
  // Code Here
  var data = clean(data)
  for (let i = 0; i < data.length; i++) {
    for (let j = 0; j < data.length; j++) {
      if (data[j] > data[j+1]) {
        var temp = data[j]
        data[j] = data[j+1]
        data[j+1] = temp
      }
    }
  }
  return data
}

// Should return array
function sortDecending(data) {
  // Code Here
  var data = clean(data)
  for (let i = 0; i < data.length; i++) {
    for (let j = 0; j < data.length; j++) {
      if (data[j] < data[j+1]) {
        var temp = data[j]
        data[j] = data[j+1]
        data[j+1] = temp
      }
    }
  }
  return data 
}

// DON'T CHANGE
test(sortAscending, sortDecending, data);


