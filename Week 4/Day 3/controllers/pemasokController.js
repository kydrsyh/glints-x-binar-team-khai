const connection = require('../models/connection.js');

class PemasokController {
    async getAll(req, res) {
        try {
            var sql = "SELECT id, nama FROM pemasok ORDER BY id"
            connection.query(sql, function (err, result) {
                if (err) {
                    res.json({
                        status: "Error",
                        error: err
                    });
                }
                res.json({
                    status: "success",
                    data: result
                })
            });
        } catch (err) {
            res.json({
                status: "Error",
                error: err
            })
        }
    }

    async getOne(req, res) {
        try {
            var sql = "SELECT id, nama FROM pemasok WHERE id = ?"
            connection.query(sql, [req.params.id], function (err, result) {
                if (err) {
                    res.json({
                        status: "Error",
                        error: err
                    });
                }
                res.json({
                    status: "success",
                    data: result[0]
                })
            });

        } catch (err) {
            res.json({
                status: "Error",
                error: err
            })
        }
    }

    async create(req, res) {
        try {
            var sql = 'INSERT INTO pemasok(nama) VALUES (?)'
            connection.query(
                sql,
                [req.body.nama],
                (err, result) => {
                    if (err) {
                        res.json({
                            status: "Error",
                            error: err
                        });
                    }
                    // res.json({
                    //     status: "Success",
                    //     data: result
                    // })

                    var sqlSelect = "SELECT id, nama FROM pemasok WHERE id = ?"

                    connection.query(sqlSelect, [result.insertId], function (err, result) {
                        if (err) {
                            res.json({
                                status: "Error",
                                error: err
                            });
                        } // If error

                        // If success it will return JSON of result
                        res.json({
                            status: "success",
                            data: result[0]
                        })
                    });
                }
            )

        } catch (err) {
            res.json({
                status: "Error",
                error: err
            })
        }
    }

    async update(req, res) {
        try {
            var sql = "UPDATE pemasok SET nama = ? WHERE id = ?"
            connection.query(
                sql,
                [req.body.nama, req.params.id],
                (err, result) => {
                    if (err) {
                        res.json({
                            status: "Error",
                            error: err
                        });
                    }
                    // res.json({
                    //     status: "Success",
                    //     data: result
                    // })
                    var sqlSelect = "SELECT id, nama FROM pemasok WHERE id = ?"

                    connection.query(sqlSelect, [req.params.id], function (err, result) {
                        if (err) {
                            res.json({
                                status: "Error",
                                error: err
                            });
                        } // If error

                        // If success it will return JSON of result
                        res.json({
                            status: "success",
                            data: result[0]
                        })
                    });
                }
            )
        } catch (err) {
            res.json({
                status: "Error",
                error: err
            })
        }
    }

    async delete(req, res) {
        try {
            var sql = "DELETE FROM pemasok WHERE id = ?"
            connection.query(
                sql,
                [req.params.id],
                (err, result) => {
                    if (err) {
                        res.json({
                            status: "Error",
                            error: err
                        });
                    }
                    res.json({
                        status: "Success",
                        data: result
                    })
                }
            )
        } catch (err) {
            res.json({
                status: "Error",
                error: err
            })
        }
    }
}


module.exports = new PemasokController