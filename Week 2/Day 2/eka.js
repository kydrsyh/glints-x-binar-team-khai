const index = require('./index.js')

function isEmptyOrSpaces(str) {
    return str === null || str.match(/^ *$/) !== null;
}

function VolumePrismaSegitiga(alas, tinggi, tinggiprisma) {
    return (1 / 2 * alas * tinggi) * tinggiprisma
}

function inputAlas() {
    index.rl.question(`Panjang Alas Segitiga: `, alas => {
        if (!isNaN(alas) && !isEmptyOrSpaces(alas)) {
            inputTinggi(alas);
        } else {
            console.log("alas must be a number !\n");
            inputAlas();
        }
    })
}

function inputTinggi(alas) {
    index.rl.question(`Tinggi Segitiga: `, tinggi => {
        if (!isNaN(tinggi) && !isEmptyOrSpaces(tinggi)) {
            inputTinggiPrisma(alas, tinggi);
        } else {
            console.log("Tinggi segitiga must be a number !\n");
            inputTinggiPrisma(tinggi);
        }
    })
}

function inputTinggiPrisma(alas, tinggi) {
    index.rl.question(`tinggi Prisma: `, tinggiprisma => {
        if (!isNaN(tinggiprisma) && !isEmptyOrSpaces(tinggiprisma)) {
            console.log(`The Prism's volume is ${VolumePrismaSegitiga(alas, tinggi, tinggiprisma)}`);
            index.rl.close()
        } else {
            console.log("tinggiprisma must be a number !\n");
            inputTinggiPrisma(alas, tinggi)
        }
    })
}
module.exports.input = inputAlas