const express = require('express')
const pemasokController = require('../controllers/pemasokController.js')
const router = express.Router()
const PemasokController = require('../controllers/pemasokController.js')

router.get('/', PemasokController.getAll)
router.get('/:id', PemasokController.getOne)
router.post('/create', pemasokController.create)
router.put('/update/:id', PemasokController.update)
router.delete('/delete/:id', PemasokController.delete)

module.exports = router
