const client = require('../models/connection.js')
const {ObjectId} = require('mongodb')

class TransaksiController {
    //Get All
    async getAll(req, res) {
        const penjualan = client.db('penjualan')
        const transaksi = penjualan.collection('transaksi')

        transaksi.find({}).toArray().
        then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    //Get One
    async getOne(req, res) {
        const penjualan = client.db('penjualan')
        const transaksi = penjualan.collection('transaksi')

        transaksi.findOne({
            _id: new ObjectId(req.params.id)
        }).then(result =>{
            res.json({
                status: "success",
                data: result
            })
        })
    }

    //Create
    async create(req,res){
        const penjualan = client.db('penjualan')
        const transaksi = penjualan.collection('transaksi')

        const barang = await penjualan.collection('barang').findOne({
            _id: new ObjectId(req.body.id_barang)
        })

        const pelanggan = await penjualan.collection('pelanggan').findOne({
            _id: new ObjectId(req.body.id_pelanggan)
        })

        let total = eval(barang.harga.toString()) * req.body.jumlah

        transaksi.insertOne({
            barang: barang,
            pelanggan: pelanggan,
            jumlah: req.body.jumlah,
            total: total
        }).then(result => {
            res.json({
                status: "success",
                data: result.ops
            })
            
        })


    }

    //Update
    async update(req, res){
        const penjualan = client.db('penjualan')
        const transaksi = penjualan.collection('transaksi')
        const barang = await penjualan.collection('barang').findOne({
            _id: new ObjectId(req.body.id_barang)
        })
        const pelanggan = await penjualan.collection('pelanggan').findOne({
            _id: new ObjectId(req.body.id_pelanggan)
        })
        let total = eval(barang.harga.toString()) * req.body.jumlah
 
        transaksi.updateOne({
            _id: new ObjectId(req.params.id)
        },{
            $set: {
                barang: barang,
                pelanggan: pelanggan,
                jumlah: req.body.jumlah,
                total: total
            }
        }).then(() => {
            return transaksi.findOne({
                _id: new ObjectId(req.params.id)
            })
        }).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async delete(req,res){
        const penjualan = client.db('penjualan')
        const transaksi = penjualan.collection('transaksi')

        transaksi.deleteOne({
            _id: new ObjectId(req.params.id)
        }).then(result =>{
            res.json({
                status: "success"
            })
        })
    }
}

module.exports = new TransaksiController