const index = require('./index.js')


function hitungVolumeTabung(pi, r, t) {
  return (pi * r ** 2) * t
}

function isEmptyOrSpaces(str) {
  return str === null || str.match(/^ *$/) !== null;
}


function inputJari() {
  index.rl.question(`Jari-jari: `, r => {
    var pi = 3.14;
    if (!isNaN(r) && !isEmptyOrSpaces(r)) {
      inputTinggi(pi, r)
    } else {
      console.log(`Jari-jari nya harus angka!\n`);
      inputJari()
    }
  })
}


function inputTinggi(pi, r) {
  index.rl.question(`Tinggi: `, t => {
    if (!isNaN(t) && !isEmptyOrSpaces(t)) {
      console.log(`Volume Tube nya adalah ${hitungVolumeTabung(pi, r, t)}`);
      index.rl.close()
    } else {
      console.log(`Tinggi nya harus angka\n`);
      inputTinggi(pi, r)
    }
  })
}




module.exports.input = inputJari