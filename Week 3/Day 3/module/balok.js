// import parent class bangunRuang
const bangunruang = require('./bangunruang.js')

class Balok extends bangunruang {
    constructor(length, width, height) {
      super('Balok')
  
      this.length = length
      this.width = width
      this.height = height
    }
  
    // Override menghitungLuas
    menghitungLuas() {
      return  2 *(this.length * this.width + this.length * this.height + this.width * this.height)
    }
  
    // Override menghitungVolume
    menghitungVolume() {
      return this.length * this.width * this.height
    }
  }
  
  module.exports = Balok;