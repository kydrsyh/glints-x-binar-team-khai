const { check, validationResult} = require('express-validator')
const client = require('../../models/connection.js')
const { ObjectId } = require('mongodb')

module.exports = {
    create: [
        check('id_barang').custom(value => {
            return client.db('penjualan').collection('barang').findOne({
                _id: new ObjectId(value)
            }).then(result => {
                if(!result) {
                    throw new Error('Id barang tidak tersedia')
                }
            })
        }),
        check('id_pelanggan').custom(value => {
            return client.db('penjualan').collection('pelanggan').findOne({
                _id: new ObjectId(value)
            }).then(result => {
                if(!result) {
                    throw new Error('Id pelanggan tidak tersedia')
                }
            })
        }),
        check('jumlah').isNumeric(),
        (req, res, next) => {
            const errors = validationResult(req)
            if(!errors.isEmpty()){
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ],
    update: [
        check('id_barang').custom(value => {
            return client.db('penjualan').collection('barang').findOne({
                _id: new ObjectId(value)
            }).then(result => {
                if(!result) {
                    throw new Error('Id barang tidak tersedia')
                }
            })
        }),
        check('id_pelanggan').custom(value => {
            return client.db('penjualan').collection('pelanggan').findOne({
                _id: new ObjectId(value)
            }).then(result => {
                if(!result) {
                    throw new Error('Id pelanggan tidak tersedia')
                }
            })
        }),
        check('jumlah').isNumeric(),
        (req, res, next) => {
            const errors = validationResult(req)
            if(!errors.isEmpty()){
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next()
        }
    ]
}