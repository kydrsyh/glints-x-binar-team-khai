const readline = require('readline')
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
})

const sleep = ms => {
    return new Promise(
      resolve => setTimeout(resolve, ms)
    )
  }
  

function heyWhoAreYou(){
    console.log("\n***************************************************");
    console.log(`Welcome to Eka, Key, Sahlan and Sherary Application`);
    console.log("***************************************************");
    rl.question(`Your name: `, name => {
        rl.question(`Job: `, job => { 
            goToAirport(name,job)
        })
    })
}
let myBag = [
    "Ticket",
    "Clothes",
    "Shoes",
    "Towel",
    "Pistol"
]

async function goToAirport(name, job) {
    console.log("\nAt the Airport");
    console.log("------------------------------------------------------------");
    console.log(`Hey ${name}, please go to the Airport`);
    console.log(`Show the ticket to security\n`);
    await sleep(500)
    checkBaggage(myBag,name,job)
}

async function checkBaggage(myBag,name,job) {
    for (let i = 0; i < myBag.length; i++) {
        if (myBag[i] == "Pistol") {
            console.log(`At Security check :`);
            console.log("------------------------------------------------------------");
            console.log("Hey, You Bring a dangerous item !!!");
            console.log("We need to bring you to the police officer\n")
            await sleep(500)
            return goToPoliceOfficer(name,job)
        }
    }
    console.log(`At Security check :`);
    console.log("------------------------------------------------------------");
    console.log("Okay, your baggage is safe")
    console.log("Pleace proceed to checkin counter\n");
    await sleep(500)
    goToCheckIn()
}


async function goToPoliceOfficer(name,job) {
    console.log("At Police Station :");
    console.log("------------------------------------------------------------");
    console.log("Please Make your clarification about this Item !!!!");
    console.log(`~~Police listen to your clarification carefully~~\n`);
    if (job.toUpperCase() == "POLICE") {
        console.log("I am sorry, you are a police officer. please continue\n");
        await sleep(500)
        goToCheckIn(name,job)
    } else {
        console.log(`Hey ${name} don't have permission to bring pistol`);
        console.log("I am sorry, Your flight is over");
        console.log("You are going to jail now !!!!")
        await sleep(500)
        rl.close()
    }
}

async function goToCheckIn(name,job) {
    console.log("At checkin counter :");
    console.log("------------------------------------------------------------");
    console.log(`Hi ${name}, Please show your id and flight ticket`);
    console.log("And drop your bag to the conveyor");
    console.log("Here is your Boarding pass, please wait in the waiting room");
    console.log("Enjoy the flight");
    await sleep(500)
    rl.close()
}

heyWhoAreYou()