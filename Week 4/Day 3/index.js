const express = require('express')
const app = express()
app.use(express.urlencoded({extended: false}))

const transaksiRoutes = require('./routes/transaksiroutes.js')
const barangRoutes = require('./routes/barangRoutes.js')
const pemasokRoutes = require('./routes/pemasokRoutes.js')
const pelangganRoutes = require('./routes/pelangganRoutes.js')

app.use('/transaksi', transaksiRoutes)
app.use('/barang', barangRoutes)
app.use('/pemasok', pemasokRoutes)
app.use('/pelanggan', pelangganRoutes)

app.listen(3000)